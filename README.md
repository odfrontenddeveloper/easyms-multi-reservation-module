<h1>EasyMS reservation engine.</h1>
<hr />
To run project:<br />
1. Use Node.js v14.17.1.<br />
2. npm i.<br />
3. npm start.<br />
4. Open http://localhost:3001/?moduleKey=d2a476c3-1d0f-4f5c-a9d3-699af50c1f9c.<br />
5. If you want to change current language open http://localhost:3001/?moduleKey=d2a476c3-1d0f-4f5c-a9d3-699af50c1f9c&lang=en. (You can use ua/en/ru as lang params).<br />
6. When you use the moduleKey for group reservation engine (with list of more than 1 hotels), user needs to choose hotel and then we can choose room. You can use one of two types of hotel choosing (like dropwdown list in line with dates, or like list of rooms under the search form). To use one of types add to link param "searchtype=dropdown".