const path = require('path')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const glob = require('glob')

module.exports = {
    mode: 'production',
    entry: {
        'bundle.js': [...glob.sync('build/static/css/main.*.?(css)'), 'build/bundle.js'].map((f) => path.resolve(__dirname, f)),
    },
    output: {
        filename: 'js/bundle.min.js',
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
        ],
    },
    plugins: [new UglifyJsPlugin()],
    performance: {
        maxEntrypointSize: 912000,
        maxAssetSize: 912000
    }
}
