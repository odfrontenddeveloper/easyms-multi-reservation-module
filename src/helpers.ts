const getparam = (field: 'moduleKey' | 'lang' | 'searchtype'): string => {
    const params: {
        moduleKey: string
        lang: string
        searchtype: string
    } = {
        moduleKey: 'data-module-key',
        lang: 'data-lang',
        searchtype: 'data-search-type',
    }

    let value: string = ''

    const searchParams = new URLSearchParams(document.location.search.substring(1))
    const getvalue: string | null = searchParams.get(field)

    if (getvalue) {
        value = getvalue
    }

    const getElement: HTMLElement | null = document.getElementById('easyms-reservation-module')

    if (getElement) {
        const attribute: string | null = getElement?.getAttribute(params[field])
        if (attribute) {
            value = attribute
        }
    }

    return value
}

export default getparam
