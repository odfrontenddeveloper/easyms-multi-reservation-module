import React from 'react'
import ReactDOM from 'react-dom'
import './index.scss'
import App from './App'
import reportWebVitals from './reportWebVitals'
import { RootStoreProvider } from 'store'
import i18n from './i18n'
import './event.ts'
import getparam from 'helpers'

const lang: string = getparam('lang')
if (lang && lang !== '') i18n.changeLanguage(lang)

ReactDOM.render(
    <RootStoreProvider>
        <App />
    </RootStoreProvider>,
    document.getElementById('easyms-reservation-module')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
