import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import detector from 'i18next-browser-languagedetector'

import translationEN from 'locales/en.json'
import translationRU from 'locales/ru.json'
import translationUK from 'locales/uk.json'

const resources = {
    en: {
        translation: translationEN,
    },
    ru: {
        translation: translationRU,
    },
    uk: {
        translation: translationUK,
    },
}

i18n.use(initReactI18next)
    .use(detector)
    .init({
        resources,
        lng: 'uk',

        interpolation: {
            escapeValue: false,
        },
    })

export default i18n
