import { imagesStore } from 'store'

document.addEventListener('keydown', (e) => {
    if (e.code === 'Escape') {
        imagesStore.setImages([])
    }
})

export {}
