import React from 'react'
import SearchRooms from 'Pages/SearchRooms/SearchRooms'
import { useStores } from 'store'
import { observer } from 'mobx-react'
import { useTranslation } from 'react-i18next'
import CustomerInfo from 'Pages/CustomerInfo/CustomerInfo'
import OrderSuccess from 'Pages/OrderSuccess/OrderSuccess'
import OrderFailure from 'Pages/OrderFailure/OrderFailure'
import './App.scss'

const SEARCH_ROOMS = 1
const CUSTOMER_INFO = 2
const ORDER_SUCCESS = 3
const ORDER_FAILURE = 4

const App = observer((): JSX.Element => {
    const { t } = useTranslation()
    const { searchRoomsStore } = useStores()

    return (
        <div className="main">
            {searchRoomsStore.step === SEARCH_ROOMS && <SearchRooms />}
            {searchRoomsStore.step === CUSTOMER_INFO && <CustomerInfo />}
            {searchRoomsStore.step === ORDER_SUCCESS && <OrderSuccess />}
            {searchRoomsStore.step === ORDER_FAILURE && <OrderFailure />}
            <div className="main-credentials">
                <a className="main-credentials-link" href="https://easyms.co/">
                    {t('credentials')}
                </a>
            </div>
        </div>
    )
})

export default App
