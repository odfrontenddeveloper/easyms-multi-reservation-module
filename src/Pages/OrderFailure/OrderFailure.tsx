import React from 'react'
import { useTranslation } from 'react-i18next'
import { Paper, Button } from '@material-ui/core'
import { HiOutlineEmojiSad } from 'react-icons/hi'
import { useStores } from 'store'
import './OrderFailure.scss'

const OrderFailure = (): JSX.Element => {
    const { t } = useTranslation()

    const { searchRoomsStore } = useStores()

    return (
        <Paper className="order-failure">
            <div className="order-failure-icon">
                <HiOutlineEmojiSad />
            </div>
            <div className="order-failure-text">{t('orderFailure.text')}</div>
            <div className="order-failure-button">
                <Button
                    color="default"
                    variant="outlined"
                    onClick={() => {
                        searchRoomsStore.setStep(1)
                    }}
                >
                    {t('customer.back')}
                </Button>
            </div>
        </Paper>
    )
}

export default OrderFailure
