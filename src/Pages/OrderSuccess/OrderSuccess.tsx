import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { Paper, Button } from '@material-ui/core'
import { IoMdCheckmarkCircleOutline } from 'react-icons/io'
import { useStores, shoppingCartStore } from 'store'
import './OrderSuccess.scss'

const OrderSuccess = (): JSX.Element => {
    const { t } = useTranslation()

    const { searchRoomsStore } = useStores()

    useEffect(() => {
        return () => {
            shoppingCartStore.resetShoppingCart()
        }
    }, [])

    return (
        <Paper className="order-success">
            <div className="order-success-icon">
                <IoMdCheckmarkCircleOutline />
            </div>
            <div className="order-success-text">{t('orderSuccess.text')}</div>
            <div className="order-success-text">
                <span>{t('orderSuccess.thankyoudescriptionone')}</span>
                <b>{shoppingCartStore.email}</b>
                <span>{t('orderSuccess.thankyoudescriptiontwo')}</span>
            </div>
            <div className="order-success-button">
                <Button
                    color="default"
                    variant="outlined"
                    onClick={() => {
                        searchRoomsStore.setStep(1)
                    }}
                >
                    {t('customer.back')}
                </Button>
            </div>
        </Paper>
    )
}

export default OrderSuccess
