import React from 'react'
import { observer } from 'mobx-react'
import { Button, Grid, Paper, TextField, FormControl, LinearProgress } from '@material-ui/core'
import ShoppingCart from 'Pages/SearchRooms/ShoppingCart/ShoppingCart'
import { MdArrowBack } from 'react-icons/md'
import { useStores } from 'store'
import { useTranslation } from 'react-i18next'
import { useForm, Controller } from 'react-hook-form'
import { createOrder, ICustomer } from 'api/requests'
import { useYupValidationResolver, validateSchema } from './validate'
import './CustomerInfo.scss'

const CustomerInfo = observer((): JSX.Element => {
    const { t } = useTranslation()

    const { availableCategoriesStore } = useStores()

    const resolver = useYupValidationResolver(validateSchema)

    const {
        control,
        handleSubmit,
        formState: { errors },
    } = useForm<{
        name: string
        email: string
        telephone: string
        remarks: string
    }>({
        resolver,
    })

    const { searchRoomsStore } = useStores()

    const onBack = () => {
        searchRoomsStore.setStep(1)
    }

    const onSubmit = (data: ICustomer) => {
        createOrder(data)
    }

    return (
        <Grid container spacing={3}>
            <Grid item lg={9} sm={12} xs={12}>
                <Paper className="customer-info">
                    <div className="customer-info-btn-container">
                        <Button variant="outlined" color="default" size="small" onClick={onBack}>
                            <MdArrowBack className="customer-info-icon" />
                            <span className="customer-info-back">{t('customer.back')}</span>
                        </Button>
                    </div>
                    <form className="customer-info-form" onSubmit={handleSubmit(onSubmit)}>
                        <FormControl className="customer-info-form-control" fullWidth>
                            <Controller
                                render={({ field: { onChange, value } }) => (
                                    <TextField
                                        id="user-name"
                                        label={t('customer.form.name')}
                                        fullWidth
                                        value={value}
                                        onChange={(e) => {
                                            onChange(e.target.value)
                                        }}
                                        error={!!errors.name}
                                        helperText={errors.name?.message && t(`customer.error.${errors.name?.message}`)}
                                    />
                                )}
                                name="name"
                                control={control}
                                defaultValue=""
                            />
                        </FormControl>
                        <FormControl className="customer-info-form-control" fullWidth>
                            <Controller
                                render={({ field: { onChange, value } }) => (
                                    <TextField
                                        id="user-email"
                                        label={t('customer.form.email')}
                                        fullWidth
                                        value={value}
                                        onChange={(e) => {
                                            onChange(e.target.value)
                                        }}
                                        error={!!errors.email}
                                        helperText={
                                            errors.email?.message && t(`customer.error.${errors.email?.message}`)
                                        }
                                    />
                                )}
                                name="email"
                                control={control}
                                defaultValue=""
                            />
                        </FormControl>
                        <FormControl className="customer-info-form-control" fullWidth>
                            <Controller
                                render={({ field: { onChange, value } }) => (
                                    <TextField
                                        id="user-phone"
                                        label={t('customer.form.telephone')}
                                        fullWidth
                                        value={value}
                                        onChange={(e) => {
                                            onChange(e.target.value)
                                        }}
                                        error={!!errors.telephone}
                                        helperText={
                                            errors.telephone?.message &&
                                            t(`customer.error.${errors.telephone?.message}`)
                                        }
                                    />
                                )}
                                name="telephone"
                                control={control}
                                defaultValue=""
                            />
                        </FormControl>
                        <FormControl className="customer-info-form-control" fullWidth>
                            <Controller
                                render={({ field: { onChange, value } }) => (
                                    <TextField
                                        multiline
                                        id="user-remarks"
                                        label={t('customer.form.remarks')}
                                        fullWidth
                                        value={value}
                                        onChange={(e) => {
                                            onChange(e.target.value)
                                        }}
                                    />
                                )}
                                name="remarks"
                                control={control}
                                defaultValue=""
                            />
                        </FormControl>
                        <FormControl className="customer-info-form-control" fullWidth>
                            <Button
                                disabled={availableCategoriesStore.isCreateOrderFetching}
                                type="submit"
                                variant="contained"
                                color="primary"
                                className="reservation-module-button"
                                size="large"
                                fullWidth
                            >
                                {t('customer.form.submit')}
                            </Button>
                        </FormControl>
                    </form>
                    <div className="linear-progress-container">
                        {availableCategoriesStore.isCreateOrderFetching && <LinearProgress color="primary" />}
                    </div>
                </Paper>
            </Grid>
            <Grid item lg={3} sm={12} xs={12}>
                <div className="shopping-cart-container">
                    <ShoppingCart total />
                </div>
            </Grid>
        </Grid>
    )
})

export default CustomerInfo
