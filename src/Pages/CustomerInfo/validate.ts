import { useCallback } from 'react'
import * as yup from 'yup'

export const validateSchema = yup.object().shape({
    name: yup.string().required('required'),
    email: yup.string().email('incorrectemail').required('required'),
    telephone: yup
        .string()
        .matches(/^\+?[0-9]\d{8,14}$/i, 'incorrectformat')
        .required('required'),
    remarks: yup.string(),
})

export const useYupValidationResolver = (validationSchema: any): any =>
    useCallback(
        async (data) => {
            try {
                const values = await validationSchema.validate(data, {
                    abortEarly: false,
                })

                return {
                    values,
                    errors: {},
                }
            } catch (errors) {
                return {
                    values: {},
                    errors: errors.inner.reduce(
                        (allErrors, currentError) => ({
                            ...allErrors,
                            [currentError.path]: {
                                type: currentError.type ?? 'validation',
                                message: currentError.message,
                            },
                        }),
                        {}
                    ),
                }
            }
        },
        [validationSchema]
    )
