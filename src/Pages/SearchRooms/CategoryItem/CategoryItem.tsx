import React, { Fragment } from 'react'
import { Grid, Paper, Button, Select, MenuItem, InputBase } from '@material-ui/core'
import { createStyles, withStyles, Theme } from '@material-ui/core/styles'
import { useTranslation } from 'react-i18next'
import { ICategoryItem } from 'store/AvailableCategoriesStore'
import { useStores } from 'store'
import { ISelectedRoom } from 'store/ShoppingCartStore'
import { placeholderImage } from 'api/urls'
import './CategoryItem.scss'

const BootstrapInput = withStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            'label + &': {
                marginTop: theme.spacing(3),
            },
        },
        input: {
            width: '100%',
            borderRadius: 4,
            position: 'relative',
            backgroundColor: theme.palette.background.paper,
            border: '1px solid #ced4da',
            fontSize: 16,
            padding: '7px 26px 7px 12px',
            transition: theme.transitions.create(['border-color', 'box-shadow']),
            // Use the system font instead of the default Roboto font.
            fontFamily: [
                '-apple-system',
                'BlinkMacSystemFont',
                '"Segoe UI"',
                'Roboto',
                '"Helvetica Neue"',
                'Arial',
                'sans-serif',
                '"Apple Color Emoji"',
                '"Segoe UI Emoji"',
                '"Segoe UI Symbol"',
            ].join(','),
            '&:focus': {
                borderRadius: 4,
                borderColor: '#80bdff',
                boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
            },
        },
    })
)(InputBase)

const CategoryItem = ({ item, isSelected }: { item: ICategoryItem; isSelected: boolean }): JSX.Element => {
    const { t } = useTranslation()
    const { shoppingCartStore, imagesStore } = useStores()

    const currentCount: ISelectedRoom | undefined = shoppingCartStore.rooms.find((room) => room.categoryId === item.id)
    const bgimage: string = item.images.length ? item.images[0] : placeholderImage

    const hamdleBookRoom = () => {
        shoppingCartStore.addRoom(item.id, 1)
    }

    const handleChangePrice = (e) => {
        const value: any = e.target.value
        const numberValue: number = +value
        if (!isNaN(numberValue)) {
            shoppingCartStore.addRoom(item.id, numberValue)
        }
    }

    const onCancel = () => {
        shoppingCartStore.removeRoom(item.id)
    }

    const description = item.description ? item.description : ''

    const parsedString = description.split('\n')

    const parsedData = parsedString.map((item, i) => (
        <Fragment key={i}>
            <p>{item}</p>
            {i + 1 >= parsedString.length && <br />}
        </Fragment>
    ))

    return (
        <Paper className="category-item">
            <Grid container spacing={2}>
                <Grid item lg={3} sm={3} xs={12}>
                    <div className="category-item-image" style={{ backgroundImage: `url(${bgimage})` }}>
                        <div
                            className="category-item-image-hover"
                            onClick={() =>
                                imagesStore.setImages(
                                    item.images.map((image) => ({
                                        original: image,
                                        thumbnail: image,
                                    }))
                                )
                            }
                        ></div>
                    </div>
                </Grid>
                <Grid item lg={9} sm={9} xs={12} className="category-item-description-column">
                    <div className="category-item-name">{item.name}</div>
                    <div className="category-item-availability">{t('common.available') + item.availability}</div>
                    <div className="category-item-description">{parsedData.map((item) => item)}</div>
                    <div>
                        <Grid container spacing={1} className="category-item-footer">
                            <Grid item lg={isSelected ? 5 : 8} sm={12} xs={12} className="category-item-footer-price">
                                {item.price + ' ' + item.currency}
                            </Grid>
                            {isSelected && (
                                <Grid item lg={3} sm={6} xs={12}>
                                    <Button
                                        variant="outlined"
                                        color="default"
                                        size="medium"
                                        fullWidth
                                        onClick={onCancel}
                                    >
                                        {t('common.cancel')}
                                    </Button>
                                </Grid>
                            )}
                            <Grid item lg={4} sm={6} xs={12}>
                                {!isSelected && (
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        className="reservation-module-button"
                                        size="medium"
                                        fullWidth
                                        onClick={hamdleBookRoom}
                                    >
                                        {t('common.booknow')}
                                    </Button>
                                )}
                                {isSelected && (
                                    <Select
                                        labelId="demo-customized-select-label"
                                        id="demo-customized-select"
                                        value={currentCount?.count || false}
                                        onChange={handleChangePrice}
                                        input={<BootstrapInput />}
                                    >
                                        {Array(item.availability)
                                            .fill(null)
                                            .map((nullitem: null, i: number) => {
                                                return (
                                                    <MenuItem key={i + 1} value={i + 1}>{`${i + 1} (${
                                                        item.price * (i + 1)
                                                    } ${item.currency})`}</MenuItem>
                                                )
                                            })}
                                    </Select>
                                )}
                            </Grid>
                        </Grid>
                    </div>
                </Grid>
            </Grid>
        </Paper>
    )
}

export default CategoryItem
