import React, { Fragment } from 'react'
import { Grid, Paper, Button } from '@material-ui/core'
import { useTranslation } from 'react-i18next'
import { searchRoomsStore, useStores } from 'store'
import { placeholderImage } from 'api/urls'
import { IProperty } from 'store/PropertiesStore'
import './CategoryItem.scss'

const HotelItem = ({ item }: { item: IProperty }): JSX.Element => {
    const { imagesStore } = useStores()

    const { t } = useTranslation()

    const bgimage: string = item.images.length ? item.images[0] : placeholderImage

    const description = item.description ? item.description : ''

    const parsedString = description.split('\n')

    const parsedData = parsedString.map((item, i) => (
        <Fragment key={i}>
            <p>{item}</p>
            {i + 1 >= parsedString.length && <br />}
        </Fragment>
    ))

    const isLocationInItem: boolean = !!item.country && !!item.city && !!item.street

    return (
        <Paper className="category-item">
            <Grid container spacing={2}>
                <Grid item lg={3} sm={3} xs={12}>
                    <div className="category-item-image" style={{ backgroundImage: `url(${bgimage})` }}>
                        <div
                            className="category-item-image-hover"
                            onClick={() =>
                                imagesStore.setImages(
                                    item.images.map((image) => ({
                                        original: image,
                                        thumbnail: image,
                                    }))
                                )
                            }
                        ></div>
                    </div>
                </Grid>
                <Grid item lg={9} sm={9} xs={12} className="category-item-description-column">
                    <div className="category-item-name">{item.name}</div>
                    <div className="category-item-address">
                        {isLocationInItem && item.country + ', ' + item.city + ', ' + item.street}
                    </div>
                    <div className="category-item-description">{parsedData.map((item) => item)}</div>
                    <div>
                        <Grid container spacing={1} className="category-item-footer">
                            <Grid item lg={4} sm={6} xs={12}>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    className="reservation-module-button"
                                    size="medium"
                                    fullWidth
                                    onClick={() => {
                                        searchRoomsStore.setSelectedHotel(item.propertyId)
                                    }}
                                >
                                    {t('selectHotel')}
                                </Button>
                            </Grid>
                        </Grid>
                    </div>
                </Grid>
            </Grid>
        </Paper>
    )
}

export default HotelItem
