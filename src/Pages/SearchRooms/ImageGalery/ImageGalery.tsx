import { observer } from 'mobx-react'
import React from 'react'
import ReactDOM from 'react-dom'
import ImageGallery from 'react-image-gallery'
import { MdClose } from 'react-icons/md'
import { useStores } from 'store'
import './ImageGalery.scss'

const ImageGalery = observer(() => {
    const element = document.getElementById('easyms-reservation-module')

    const { imagesStore } = useStores()

    if (!element || !imagesStore.images.length) return null

    return ReactDOM.createPortal(
        <div className="custom-image-gallery">
            <div className="custom-image-gallery-clickout">
                <div className="custom-image-gallery-clickout-close" onClick={() => imagesStore.setImages([])}>
                    <MdClose />
                </div>
            </div>
            <div>
                <ImageGallery items={imagesStore.images} />
            </div>
        </div>,
        element
    )
})

export default ImageGalery
