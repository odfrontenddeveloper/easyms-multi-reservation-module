import React from 'react'
import { Paper, Divider, Button, FormControl } from '@material-ui/core'
import './ShoppingCart.scss'
import { observer } from 'mobx-react'
import { useTranslation } from 'react-i18next'
import { pricesStore, useStores } from 'store'
import { IShoppingCartCategory } from 'store/AvailableCategoriesStore'

const ShoppingCart = observer(({ total }: { total?: boolean }): JSX.Element => {
    const { t } = useTranslation()
    const { searchRoomsStore, availableCategoriesStore, shoppingCartStore } = useStores()

    const onContinue = () => {
        searchRoomsStore.setStep(2)
    }

    return (
        <Paper className="shopping-cart">
            <div className="shopping-cart-header">{t('shoppingCart.booking')}</div>
            <Divider />
            <div className="shopping-cart-rows">
                <div className="shopping-cart-row">
                    <span className="shopping-cart-row-label">{t('shoppingCart.arrival')}</span>
                    <span className="shopping-cart-row-value">
                        {searchRoomsStore.dates.arrivalDate.format('DD.MM.yyyy')}
                    </span>
                </div>
                <div className="shopping-cart-row">
                    <span className="shopping-cart-row-label">{t('shoppingCart.departure')}</span>
                    <span className="shopping-cart-row-value">
                        {searchRoomsStore.dates.departureDate.format('DD.MM.yyyy')}
                    </span>
                </div>
                <div className="shopping-cart-row">
                    <span className="shopping-cart-row-label">{t('shoppingCart.nights')}</span>
                    <span className="shopping-cart-row-value">{searchRoomsStore.selectDatesdifference}</span>
                </div>
            </div>
            <Divider />
            {availableCategoriesStore.selectShoppingCartData.map((item: IShoppingCartCategory) => {
                return (
                    <div key={item.id} className="shopping-cart-selected-room">
                        <div className="shopping-cart-selected-room-name">{item.name}</div>
                        <div className="shopping-cart-selected-room-flex">
                            <div className="shopping-cart-selected-room-count">
                                <div>{t('shoppingCart.count')}</div>
                                <div>{item.count + ' x ' + item.price / item.count + ' ' + item.currency}</div>
                            </div>
                            <div className="shopping-cart-selected-room-price">
                                <div>{t('shoppingCart.total')}</div>
                                <div>{item.price + ' ' + item.currency}</div>
                            </div>
                        </div>
                    </div>
                )
            })}
            {!shoppingCartStore.rooms.length && (
                <div className="shopping-cart-nothing">{t('shoppingCart.norooms')}</div>
            )}
            <Divider />
            {total && pricesStore.prices && pricesStore.prices.currency && (
                <div className="shopping-cart-total-invoice">
                    <div className="shopping-cart-total-invoice-total">{t('shoppingCart.total')}</div>
                    <div className="shopping-cart-total-invoice-value">
                        {availableCategoriesStore.selectShoppingCartTotalInvoice + ' ' + pricesStore.prices.currency}
                    </div>
                </div>
            )}
            {!total && (
                <FormControl className="shopping-cart-button-container" fullWidth>
                    <Button
                        variant="contained"
                        color="primary"
                        className="reservation-module-button"
                        size="small"
                        disabled={!availableCategoriesStore.selectShoppingCartData.length}
                        onClick={onContinue}
                        fullWidth
                    >
                        {t('common.continue')}
                    </Button>
                </FormControl>
            )}
        </Paper>
    )
})

export default ShoppingCart
