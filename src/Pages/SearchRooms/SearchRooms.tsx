import React from 'react'
import { observer } from 'mobx-react'
import { Grid, Container, Button } from '@material-ui/core'
import { useTranslation } from 'react-i18next'
import { useStores } from 'store'
import SearchRoomsForm from './SearchRoomsForm/SearchRoomsForm'
import { ICategoryItem } from 'store/AvailableCategoriesStore'
import ShoppingCart from 'Pages/SearchRooms/ShoppingCart/ShoppingCart'
import CategoryItem from './CategoryItem/CategoryItem'
import HotelItem from './CategoryItem/HotelItem'
import ImageGallery from './ImageGalery/ImageGalery'
import './SearchRooms.scss'
import getparam from 'helpers'

const gettype: string = getparam('searchtype')

const SearchRooms = observer((): JSX.Element => {
    const { t } = useTranslation()
    const { availableCategoriesStore, searchRoomsStore, shoppingCartStore, propertiesStore } = useStores()

    const useDropdown: boolean = gettype === 'dropdown'

    const canRenderError: boolean =
        !availableCategoriesStore.selectCategories.length &&
        availableCategoriesStore.firstFetchCompleted &&
        !availableCategoriesStore.isErrorSomething &&
        !availableCategoriesStore.isFetchSomething

    const canRenderHotelItems: boolean =
        !searchRoomsStore.selectedHotel &&
        !useDropdown &&
        searchRoomsStore.departureSelectedAlready &&
        searchRoomsStore.arrivalSelectedAlready

    return (
        <div className="search-rooms-container">
            <SearchRoomsForm />
            {!!searchRoomsStore.selectedHotel && !useDropdown && propertiesStore.properties.length > 1 && (
                <Grid container spacing={2}>
                    <Grid item lg={4} sm={3} xs={12}>
                        <div className="back-to-choose">
                            <Button
                                variant="contained"
                                color="primary"
                                className="reservation-module-button"
                                size="small"
                                onClick={() => searchRoomsStore.setSelectedHotel(null)}
                            >
                                {t('hotelChoose')}
                            </Button>
                        </div>
                    </Grid>
                    <Grid item lg={4} sm={3} xs={12}>
                        <div className="selected-hotel">
                            {t('searchRooms.hotel') + ': '}
                            <b>{searchRoomsStore.currentHotelName}</b>
                        </div>
                    </Grid>
                    <Grid item lg={4} sm={3} xs={12}></Grid>
                </Grid>
            )}
            <Container>
                {availableCategoriesStore.isErrorSomething && (
                    <div className="search-rooms-container-message">{t('searchRooms.error.fetch')}</div>
                )}
                {canRenderError && <div className="search-rooms-container-message">{t('searchRooms.error.rooms')}</div>}
                {canRenderHotelItems &&
                    propertiesStore.properties.map((item) => <HotelItem key={item.propertyId} item={item} />)}
                {!!availableCategoriesStore.selectCategories.length ? (
                    <Grid container spacing={2}>
                        <Grid item lg={9} sm={12} xs={12}>
                            {searchRoomsStore.selectedHotel &&
                                availableCategoriesStore.selectCategories.map((item: ICategoryItem) => {
                                    const isSelected: boolean = shoppingCartStore.rooms.some(
                                        (room) => room.categoryId === item.id
                                    )
                                    return <CategoryItem key={item.id} isSelected={isSelected} item={item} />
                                })}
                        </Grid>
                        <Grid item lg={3} sm={12} xs={12} className="relative-container">
                            {searchRoomsStore.selectedHotel && <ShoppingCart />}
                        </Grid>
                    </Grid>
                ) : (
                    <Grid container spacing={2}>
                        <Grid item lg={12} sm={12} xs={12}></Grid>
                    </Grid>
                )}
            </Container>
            <ImageGallery />
        </div>
    )
})

export default SearchRooms
