import React, { useEffect } from 'react'
import { observer } from 'mobx-react'
import { Paper, Grid, Button, FormControl, Select, MenuItem, InputLabel, LinearProgress } from '@material-ui/core'
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers'
import { useTranslation } from 'react-i18next'
import './SearchRoomsForm.scss'
import { useStores } from 'store'
import { fetchDescriptions, fetchPrices, fetchProperties, fetchCategories } from 'api/requests'
import { IProperty } from 'store/PropertiesStore'
import MomentUtils from '@date-io/moment'
import moment, { Moment } from 'moment'
import 'moment/locale/ru'
import 'moment/locale/en-au'
import 'moment/locale/uk'
import i18n from 'i18n'
import getparam from 'helpers'

const gettype: string = getparam('searchtype')

const SearchRooms = observer((): JSX.Element => {
    const { t } = useTranslation()
    const { searchRoomsStore, propertiesStore, availableCategoriesStore, categoryDescriptionsStore, pricesStore } =
        useStores()

    const { arrivalSelectedAlready, departureSelectedAlready, selectedHotel, dates } = searchRoomsStore

    const { arrivalDate, departureDate } = dates

    const arrivalFormat: string = arrivalDate.format('DD.MM.yyyy')
    const departureFormat: string = departureDate.format('DD.MM.yyyy')

    const handleSearch = (): void => {
        fetchCategories()
        fetchPrices()
        fetchDescriptions()
    }

    useEffect(() => {
        fetchProperties()
    }, [])

    useEffect(() => {
        !!arrivalSelectedAlready && !!departureSelectedAlready && !!selectedHotel && handleSearch()
    }, [arrivalSelectedAlready, departureSelectedAlready, selectedHotel, arrivalFormat, departureFormat])

    const sometingIsFetching: boolean = [availableCategoriesStore, categoryDescriptionsStore, pricesStore].some(
        (store) => store.isFetching
    )

    const handleSelectHotel = (e: any) => {
        searchRoomsStore.setSelectedHotel(+e.target.value)
    }

    const handleChangeArrival = (date: Moment | null): void => {
        if (!!date && !!searchRoomsStore?.dates?.departureDate) {
            const changeDate: Moment = moment(date).set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
            const diff: number = searchRoomsStore.dates.departureDate.diff(date)
            if (diff < 86400000) {
                const newDeparture: Moment = moment(changeDate).add(1, 'days')
                searchRoomsStore.setArrivalSelectedAlready(true)
                searchRoomsStore.setDates(changeDate, newDeparture)
            } else {
                searchRoomsStore.setArrivalSelectedAlready(true)
                searchRoomsStore.setDates(changeDate, false)
            }
        }
    }

    const handleChangeDeparture = (date: Moment | null): void => {
        if (!!date) {
            searchRoomsStore.setDepartureSelectedAlready(true)
            searchRoomsStore.setDates(false, moment(date).set({ hour: 0, minute: 0, second: 0, millisecond: 0 }))
        }
    }

    const arrivalDatesDisabled = (date: Moment | null): boolean => {
        const today = moment().set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
        if (!!searchRoomsStore?.dates?.arrivalDate && !!date) {
            const changeDate: Moment = moment(date).set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
            const diff: number = today.diff(changeDate)
            return diff > 0
        }
        return false
    }

    const departureDatesDisabled = (date: Moment | null): boolean => {
        if (!!searchRoomsStore?.dates?.arrivalDate && !!date) {
            const changeDate: Moment = moment(date).set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
            const diff: number = searchRoomsStore.dates.arrivalDate.diff(changeDate)
            return diff > -86400000
        }
        return false
    }

    const useDropdown: boolean = gettype === 'dropdown' && propertiesStore.properties.length > 1

    return (
        <Paper className="paper-customization">
            <Grid container spacing={7}>
                {useDropdown && (
                    <Grid item lg={3} sm={12} xs={12}>
                        <FormControl fullWidth>
                            <InputLabel shrink id="demo-simple-select-label">
                                {t('searchRooms.hotel')}
                            </InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={searchRoomsStore.selectedHotel || ''}
                                onChange={handleSelectHotel}
                                displayEmpty
                            >
                                {propertiesStore.properties.map((option: IProperty) => (
                                    <MenuItem value={option.propertyId} key={option.propertyId}>
                                        {option.name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                )}
                <Grid item lg={useDropdown ? 3 : 4} sm={12} xs={12}>
                    <FormControl fullWidth>
                        <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={i18n.language}>
                            <KeyboardDatePicker
                                disableToolbar
                                variant="inline"
                                format="DD/MM/yyyy"
                                id="date-arrival"
                                label={t('searchRooms.arrival')}
                                value={searchRoomsStore.dates.arrivalDate}
                                onChange={handleChangeArrival}
                                shouldDisableDate={arrivalDatesDisabled}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </FormControl>
                </Grid>
                <Grid item lg={useDropdown ? 3 : 4} sm={12} xs={12}>
                    <FormControl fullWidth>
                        <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={i18n.language}>
                            <KeyboardDatePicker
                                disableToolbar
                                variant="inline"
                                format="DD/MM/yyyy"
                                id="date-departure"
                                label={t('searchRooms.departure')}
                                value={searchRoomsStore.dates.departureDate}
                                onChange={handleChangeDeparture}
                                shouldDisableDate={departureDatesDisabled}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </FormControl>
                </Grid>
                <Grid item lg={useDropdown ? 3 : 4} sm={12} xs={12}>
                    <FormControl fullWidth className="button-container">
                        <Button
                            variant="contained"
                            color="primary"
                            className="reservation-module-button"
                            size="large"
                            fullWidth
                            disabled={!searchRoomsStore.selectedHotel}
                            onClick={() => {
                                searchRoomsStore.setArrivalSelectedAlready(true)
                                searchRoomsStore.setDepartureSelectedAlready(true)
                                if (!availableCategoriesStore.firstFetchCompleted) {
                                    availableCategoriesStore.setFirstFetchCompleted(true)
                                } else {
                                    handleSearch()
                                }
                            }}
                        >
                            {t('searchRooms.submit')}
                        </Button>
                    </FormControl>
                </Grid>
            </Grid>
            <div className="linear-progress-container">{sometingIsFetching && <LinearProgress color="primary" />}</div>
        </Paper>
    )
})

export default SearchRooms
