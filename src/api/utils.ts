import { Moment } from 'moment'

const formatErrorMessage = (res) => `[${res.status}]: ${res.statusText} (${res.url})`

const isJsonResponse = (response) => {
    const contentType = response.headers.get('content-type')
    return contentType && (contentType.includes('application/json') || contentType.includes('application/hal+json'))
}

export const handleResponse = async (response: Response): Promise<any> => {
    if (response.ok) {
        if (isJsonResponse(response)) {
            return response.json()
        } else {
            return {}
        }
    } else {
        const error = new Error(formatErrorMessage(response))
        throw error
    }
}

export const getUnixTime = (date: Moment): number => date.utc(true).unix() * 1000

export const fetchUrl = {
    get: async (url: string, query?: object): Promise<Response> => {
        if (query) {
            const queryOptions = Object.keys(query)
                .map((item) => {
                    return item + '=' + query[item]
                })
                .join('&')

            url = url + '?' + queryOptions
        }

        const options: RequestInit = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }

        return fetch(url, options)
    },
    post: async (url: string, body?: object, query?: object): Promise<Response> => {
        const options: RequestInit = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: body ? JSON.stringify(body) : null,
        }

        if (query) {
            const queryOptions = Object.keys(query)
                .map((item) => {
                    return item + '=' + query[item]
                })
                .join('&')

            url = url + '?' + queryOptions
        }

        return fetch(url, options)
    },
}
