export const url: string = process.env.REACT_APP_APILINK || ''

export const getPropertiesUrl: (key: string) => string = (key: string) => `${url}/api/reservation/pub/${key}/properties`
export const availableCategoriesUrl: (key: string) => string = (key: string) =>
    `${url}/api/reservation/pub/${key}/availableCategories`
export const categoriesDescriptionsUrl: (key: string) => string = (key: string) =>
    `${url}/api/reservation/pub/${key}/categoryDescriptions`
export const pricesUrl: (key: string) => string = (key: string) => `${url}/api/reservation/pub/${key}/prices`
export const createOrderUrl: (key: string, propertyId: number) => string = (key: string, propertyId: number) => {
    return `${url}/api/reservation/pub/${key}/createOrder?propertyId=${propertyId}`
}
export const placeholderImage =
    'https://easyms-gallery.ams3.digitaloceanspaces.com/4b20e2f8-c6dc-4dc5-9821-b6a5e63642e3'
