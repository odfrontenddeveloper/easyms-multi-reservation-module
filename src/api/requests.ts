import { IAvailableCategory } from 'store/AvailableCategoriesStore'
import { ICategoryDescription } from 'store/CategortyDescriptionsStore'
import { IPrice } from 'store/PricesStore'
import { IProperty } from 'store/PropertiesStore'
import { getPropertiesUrl, availableCategoriesUrl, categoriesDescriptionsUrl, pricesUrl, createOrderUrl } from './urls'
import { fetchUrl, getUnixTime, handleResponse } from './utils'
import {
    availableCategoriesStore,
    categoryDescriptionsStore,
    pricesStore,
    propertiesStore,
    searchRoomsStore,
    shoppingCartStore,
} from 'store'
import getparam from 'helpers'

const key: string = getparam('moduleKey')

export interface ICustomer {
    name: string
    email: string
    telephone: string
    remarks: string
}

export interface IRoom {
    arrival: number
    departure: number
    categoryId: number
    invoice: number
}

export interface IOrder {
    customer: ICustomer
    rooms: IRoom[]
}

export interface IOrderCreated {
    redirectUrl: string
}

export const fetchProperties = (): void => {
    propertiesStore.setProperties([])
    propertiesStore.setIsFetching(true)
    fetchUrl
        .get(getPropertiesUrl(key))
        .then(handleResponse)
        .then((properties: IProperty[]) => {
            propertiesStore.setIsFetching(false)
            propertiesStore.setProperties(properties)
            if (properties.length === 1) {
                searchRoomsStore.setSelectedHotel(properties[0].propertyId)
            }
        })
        .catch(() => {
            propertiesStore.setIsFetching(false)
        })
}

export const fetchCategories = (): void => {
    const { dates, selectedHotel } = searchRoomsStore

    const { arrivalDate, departureDate } = dates

    const startTime: number = getUnixTime(arrivalDate)
    const endTime: number = getUnixTime(departureDate)

    const query = {
        propertyId: selectedHotel,
        startTime,
        endTime,
    }

    shoppingCartStore.resetShoppingCart()
    availableCategoriesStore.setError(false)
    availableCategoriesStore.setAvailableCategories([])
    availableCategoriesStore.setIsFetching(true)
    availableCategoriesStore.setFirstFetchCompleted(true)
    fetchUrl
        .get(availableCategoriesUrl(key), query)
        .then(handleResponse)
        .then((data: IAvailableCategory[]) => {
            availableCategoriesStore.setIsFetching(false)
            availableCategoriesStore.setAvailableCategories(data)
        })
        .catch(() => {
            availableCategoriesStore.setError(true)
            availableCategoriesStore.setIsFetching(false)
        })
}

export const fetchDescriptions = (): void => {
    const { selectedHotel } = searchRoomsStore

    const query = {
        propertyId: selectedHotel,
    }

    categoryDescriptionsStore.setError(false)
    categoryDescriptionsStore.setCategoryDescriptions([])
    categoryDescriptionsStore.setIsFetching(true)
    fetchUrl
        .get(categoriesDescriptionsUrl(key), query)
        .then(handleResponse)
        .then((data: ICategoryDescription[]) => {
            categoryDescriptionsStore.setCategoryDescriptions(data)
            categoryDescriptionsStore.setIsFetching(false)
        })
        .catch(() => {
            categoryDescriptionsStore.setError(true)
            categoryDescriptionsStore.setIsFetching(false)
        })
}

export const fetchPrices = (): void => {
    const { dates, selectedHotel } = searchRoomsStore

    const { arrivalDate, departureDate } = dates

    const startTime: number = getUnixTime(arrivalDate)
    const endTime: number = getUnixTime(departureDate)

    const query = {
        propertyId: selectedHotel,
        startTime,
        endTime,
    }

    pricesStore.setError(false)
    pricesStore.setPrices({})
    pricesStore.setIsFetching(true)
    fetchUrl
        .get(pricesUrl(key), query)
        .then(handleResponse)
        .then((data: IPrice) => {
            if (!!data) {
                pricesStore.setPrices({
                    ...data,
                    prices: data.prices.filter((item) => !!item.value),
                })
            }
            pricesStore.setIsFetching(false)
        })
        .catch(() => {
            pricesStore.setError(true)
            pricesStore.setIsFetching(false)
        })
}

export const createOrder = ({ name, email, telephone, remarks }: ICustomer): void => {
    const customer: ICustomer = {
        name,
        email,
        telephone,
        remarks,
    }

    const { dates, selectedHotel } = searchRoomsStore

    const { arrivalDate, departureDate } = dates

    const rooms: IRoom[] = availableCategoriesStore.selectShoppingCartData
        .map((item) => {
            return Array(item.count).fill({
                arrival: getUnixTime(arrivalDate),
                departure: getUnixTime(departureDate),
                categoryId: item.id,
                invoice: item.price / item.count,
            })
        })
        .reduce((prev, next) => {
            return [...prev, ...next]
        }, [])

    const order: IOrder = {
        customer,
        rooms,
    }

    if (selectedHotel) {
        availableCategoriesStore.setIsCreateOrderFetching(true)
        fetchUrl
            .post(createOrderUrl(key, selectedHotel), order)
            .then(handleResponse)
            .then((data: IOrderCreated) => {
                availableCategoriesStore.setIsCreateOrderFetching(false)
                shoppingCartStore.resetShoppingCart()
                shoppingCartStore.setEmail(email)
                searchRoomsStore.setStep(3)
                if (data.redirectUrl)
                    setTimeout(() => {
                        window.location.href = data.redirectUrl
                    }, 1000)
            })
            .catch(() => {
                availableCategoriesStore.setIsCreateOrderFetching(false)
                shoppingCartStore.resetShoppingCart()
                searchRoomsStore.setStep(4)
            })
    }
}
