import React from 'react'
import { SearchRooms } from './SearchRoomsStore'
import { Properties } from './PropertiesStore'
import { AvailableCategories } from './AvailableCategoriesStore'
import { CategoryDescriptions } from './CategortyDescriptionsStore'
import { ShoppingCart } from './ShoppingCartStore'
import { Prices } from './PricesStore'
import { Images } from './ImagesStore'

interface IRootStore {
    searchRoomsStore: SearchRooms
    propertiesStore: Properties
    availableCategoriesStore: AvailableCategories
    categoryDescriptionsStore: CategoryDescriptions
    pricesStore: Prices
    shoppingCartStore: ShoppingCart
    imagesStore: Images
}

const RootStoreContext = React.createContext<IRootStore>({} as IRootStore)

export const searchRoomsStore = new SearchRooms()
export const propertiesStore = new Properties()
export const availableCategoriesStore = new AvailableCategories()
export const categoryDescriptionsStore = new CategoryDescriptions()
export const pricesStore = new Prices()
export const shoppingCartStore = new ShoppingCart()
export const imagesStore = new Images()

export const RootStoreProvider = ({ children }: React.PropsWithChildren<{}>): JSX.Element => {
    return (
        <RootStoreContext.Provider
            value={{
                searchRoomsStore,
                propertiesStore,
                availableCategoriesStore,
                categoryDescriptionsStore,
                pricesStore,
                shoppingCartStore,
                imagesStore,
            }}
        >
            {children}
        </RootStoreContext.Provider>
    )
}

export const useStores = (): IRootStore => React.useContext(RootStoreContext)
