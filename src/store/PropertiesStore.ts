import { action, makeObservable, observable } from 'mobx'

export interface IProperty {
    propertyId: number
    name: string
    description: string
    country: string
    city: string
    street: string
    longitude: number
    latitude: number
    images: string[]
    active: boolean
}

export class Properties {
    isFetching: boolean = false
    properties: IProperty[] = []

    constructor() {
        makeObservable(this, {
            isFetching: observable,
            properties: observable,
            setIsFetching: action,
            setProperties: action,
        })

        this.setIsFetching = this.setIsFetching.bind(this)
        this.setProperties = this.setProperties.bind(this)
    }

    setIsFetching(isFetching: boolean): void {
        this.isFetching = isFetching
    }

    setProperties(properties: IProperty[]): void {
        this.properties = properties
    }
}
