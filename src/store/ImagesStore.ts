import { makeObservable, observable, action } from 'mobx'

export interface IImage {
    original: string
    thumbnail: string
}

export class Images {
    images: IImage[] = []

    constructor() {
        makeObservable(this, {
            images: observable,
            setImages: action,
        })

        this.setImages = this.setImages.bind(this)
    }

    setImages(images: IImage[]): void {
        this.images = images
    }
}
