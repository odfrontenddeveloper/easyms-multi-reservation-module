import { observable, action, makeObservable } from 'mobx'

export interface ISelectedRoom {
    categoryId: number
    count: number
}

export class ShoppingCart {
    rooms: ISelectedRoom[] = []

    email: string | null = null

    constructor() {
        makeObservable(this, {
            rooms: observable,
            email: observable,
            addRoom: action,
            resetShoppingCart: action,
            removeRoom: action,
            setEmail: action,
        })

        this.setEmail = this.setEmail.bind(this)
        this.addRoom = this.addRoom.bind(this)
        this.removeRoom = this.removeRoom.bind(this)
        this.resetShoppingCart = this.resetShoppingCart.bind(this)
    }

    setEmail(email: string | null): void {
        this.email = email
    }

    addRoom(categoryId: number, count: number): void {
        const findRoom: ISelectedRoom | undefined = this.rooms.find(
            (room: ISelectedRoom) => room.categoryId === categoryId
        )
        if (findRoom) {
            this.rooms = this.rooms.map((item: ISelectedRoom) => {
                if (item.categoryId === categoryId) {
                    return {
                        categoryId,
                        count,
                    }
                }
                return item
            })
        } else {
            this.rooms.push({
                categoryId,
                count,
            })
        }
    }

    removeRoom(categoryId: number): void {
        this.rooms = this.rooms.filter((item) => item.categoryId !== categoryId)
    }

    resetShoppingCart(): void {
        this.email = null
        this.rooms = []
    }
}
