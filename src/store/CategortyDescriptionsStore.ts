import { action, makeObservable, observable } from 'mobx'

export interface ICategoryDescription {
    active: boolean
    categoryId: number
    description: string
    images: string[]
    name: string
}

export class CategoryDescriptions {
    isFetching: boolean = false
    categoryDescription: ICategoryDescription[] = []

    hasError: boolean = false

    constructor() {
        makeObservable(this, {
            isFetching: observable,
            categoryDescription: observable,
            hasError: observable,
            setError: action,
            setIsFetching: action,
            setCategoryDescriptions: action,
        })

        this.setError = this.setError.bind(this)
        this.setIsFetching = this.setIsFetching.bind(this)
        this.setCategoryDescriptions = this.setCategoryDescriptions.bind(this)
    }

    setError(error: boolean) {
        this.hasError = error
    }

    setIsFetching(isFetching: boolean): void {
        this.isFetching = isFetching
    }

    setCategoryDescriptions(categories: ICategoryDescription[]): void {
        this.categoryDescription = categories
    }
}
