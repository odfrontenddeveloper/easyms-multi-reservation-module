import { action, makeObservable, observable, computed } from 'mobx'
import { categoryDescriptionsStore, pricesStore, shoppingCartStore } from 'store'
import { intersection } from 'lodash'
import { ICategoryDescription } from './CategortyDescriptionsStore'
import { IPrice } from './PricesStore'
import { ISelectedRoom } from './ShoppingCartStore'

export interface IAvailableCategory {
    availability: number
    categoryId: number
    categoryName: string
}

export interface ICategoryItem {
    id: number
    availability: number
    name: string
    description: string
    images: string[]
    price: number
    currency: string
}

export interface IShoppingCartCategory {
    id: number
    name: string
    count: number
    price: number
    currency: string
}

export class AvailableCategories {
    isFetching: boolean = false
    availableCategories: IAvailableCategory[] = []

    hasError: boolean = false

    firstFetchCompleted: boolean = false

    isCreateOrderFetching: boolean = false

    constructor() {
        makeObservable(this, {
            isCreateOrderFetching: observable,
            isFetching: observable,
            availableCategories: observable,
            hasError: observable,
            firstFetchCompleted: observable,
            setError: action,
            setFirstFetchCompleted: action,
            setIsFetching: action,
            setAvailableCategories: action,
            setIsCreateOrderFetching: action,
            selectCategories: computed,
            selectShoppingCartData: computed,
            selectShoppingCartTotalInvoice: computed,
            isFetchSomething: computed,
            isErrorSomething: computed,
        })

        this.setIsCreateOrderFetching = this.setIsCreateOrderFetching.bind(this)
        this.setError = this.setError.bind(this)
        this.setFirstFetchCompleted = this.setFirstFetchCompleted.bind(this)
        this.setIsFetching = this.setIsFetching.bind(this)
        this.setAvailableCategories = this.setAvailableCategories.bind(this)
    }

    setIsCreateOrderFetching(isFetching: boolean): void {
        this.isCreateOrderFetching = isFetching
    }

    setError(error: boolean): void {
        this.hasError = error
    }

    setFirstFetchCompleted(fetch: boolean): void {
        this.firstFetchCompleted = fetch
    }

    setIsFetching(isFetching: boolean): void {
        this.isFetching = isFetching
    }

    setAvailableCategories(categories: IAvailableCategory[]): void {
        this.availableCategories = categories
    }

    get isFetchSomething(): boolean {
        const isPricesFetching: boolean = pricesStore.isFetching
        const isDescriptionsFetching: boolean = categoryDescriptionsStore.isFetching

        return isPricesFetching || isDescriptionsFetching || this.isFetching
    }

    get isErrorSomething(): boolean {
        const isPricesError: boolean = pricesStore.hasError
        const isDescriptionsError: boolean = categoryDescriptionsStore.hasError

        return isPricesError || isDescriptionsError || this.hasError
    }

    get selectCategories(): ICategoryItem[] {
        const prices: IPrice = pricesStore.prices
        const descriptions: ICategoryDescription[] = categoryDescriptionsStore.categoryDescription

        if (!!prices.prices && !!prices.prices.length && !!descriptions.length && !!this.availableCategories.length) {
            const pricesIds: number[] = prices.prices ? prices.prices.map((item) => item.categoryId) : []
            const descriptionsIds: number[] = descriptions.filter((item) => item.active).map((item) => item.categoryId)
            const categoriesIds: number[] = this.availableCategories.map((item) => item.categoryId)

            const intersectionIds: number[] = intersection(pricesIds, descriptionsIds, categoriesIds)

            return intersectionIds.sort().map((id: number) => {
                const findPrice: { value: number; categoryId: number } | undefined = prices.prices.find(
                    (item) => item.categoryId === id
                )

                const findDescription: ICategoryDescription | undefined = descriptions.find(
                    (item) => item.categoryId === id
                )

                const findCategory: IAvailableCategory | undefined = this.availableCategories.find(
                    (item) => item.categoryId === id
                )

                return {
                    id,
                    availability: findCategory?.availability || 0,
                    name: findDescription?.name || '',
                    description: findDescription?.description || '',
                    images: findDescription?.images || [],
                    price: findPrice?.value || 0,
                    currency: prices.currency,
                }
            })
        }
        return []
    }

    get selectShoppingCartData(): IShoppingCartCategory[] {
        const shoppingCartData: ISelectedRoom[] = shoppingCartStore.rooms
        const descriptions: ICategoryDescription[] = categoryDescriptionsStore.categoryDescription.filter(
            (item) => item.active
        )
        const prices: IPrice = pricesStore.prices

        const result: IShoppingCartCategory[] = shoppingCartData.map((item: ISelectedRoom) => {
            const findDescription: ICategoryDescription | undefined = descriptions.find(
                (description) => description.categoryId === item.categoryId
            )

            const findPrice: { value: number; categoryId: number } | undefined = prices.prices.find(
                (price) => price.categoryId === item.categoryId
            )

            return {
                id: item.categoryId,
                name: findDescription?.name || '',
                count: item.count,
                price: findPrice ? findPrice.value * item.count : 0,
                currency: prices.currency,
            }
        })

        return result
    }

    get selectShoppingCartTotalInvoice(): number {
        const shoppingCartData: ISelectedRoom[] = shoppingCartStore.rooms
        const prices: IPrice = pricesStore.prices

        const result: number = shoppingCartData
            .map((item: ISelectedRoom) => {
                const findPrice: { value: number; categoryId: number } | undefined = prices.prices.find(
                    (price) => price.categoryId === item.categoryId
                )

                return findPrice ? findPrice.value * item.count : 0
            })
            .reduce((prev, next) => prev + next, 0)

        return 0 + result
    }
}
