import { action, makeObservable, observable } from 'mobx'

export interface IPrice {
    currency: string
    prices: {
        categoryId: number
        value: number
    }[]
    rateId: number
}

export class Prices {
    isFetching: boolean = false
    prices: IPrice = {} as IPrice

    hasError: boolean = false

    constructor() {
        makeObservable(this, {
            isFetching: observable,
            prices: observable,
            hasError: observable,
            setError: action,
            setIsFetching: action,
            setPrices: action,
        })

        this.setError = this.setError.bind(this)
        this.setIsFetching = this.setIsFetching.bind(this)
        this.setPrices = this.setPrices.bind(this)
    }

    setError(error: boolean): void {
        this.hasError = error
    }

    setIsFetching(isFetching: boolean): void {
        this.isFetching = isFetching
    }

    setPrices(prices: IPrice | {}): void {
        const pricesData = prices as IPrice
        this.prices = pricesData
    }
}
