import { action, makeObservable, observable, computed } from 'mobx'
import moment, { Moment } from 'moment'
import { propertiesStore, shoppingCartStore } from 'store'
import { IProperty } from './PropertiesStore'

const today = moment().set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
const nextDay = moment().set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).add(1, 'days')

export class SearchRooms {
    step: 1 | 2 | 3 | 4 = 1

    arrivalSelectedAlready: boolean = false
    departureSelectedAlready: boolean = false

    selectedHotel: number | null = null
    dates: {
        arrivalDate: Moment
        departureDate: Moment
    } = {
        arrivalDate: today,
        departureDate: nextDay,
    }

    constructor() {
        makeObservable(this, {
            step: observable,
            arrivalSelectedAlready: observable,
            departureSelectedAlready: observable,
            selectedHotel: observable,
            dates: observable,
            setStep: action,
            setSelectedHotel: action,
            setDates: action,
            setArrivalSelectedAlready: action,
            setDepartureSelectedAlready: action,
            selectDatesdifference: computed,
            currentHotelName: computed,
        })

        this.setStep = this.setStep.bind(this)
        this.setSelectedHotel = this.setSelectedHotel.bind(this)
        this.setDates = this.setDates.bind(this)
        this.setArrivalSelectedAlready = this.setArrivalSelectedAlready.bind(this)
        this.setDepartureSelectedAlready = this.setDepartureSelectedAlready.bind(this)
    }

    get selectDatesdifference(): number {
        return this.dates.departureDate.diff(this.dates.arrivalDate, 'days')
    }

    get currentHotelName(): string {
        const findHotel: IProperty | undefined = propertiesStore.properties.find(
            (item) => item.propertyId === this.selectedHotel
        )
        return findHotel?.name || ''
    }

    setStep(step: 1 | 2 | 3 | 4): void {
        this.step = step
    }

    setSelectedHotel(hotelId: number | null): void {
        shoppingCartStore.resetShoppingCart()
        this.selectedHotel = hotelId
    }

    setDates(arrival: Moment | false, departure: Moment | false): void {
        this.dates = {
            arrivalDate: arrival || this.dates.arrivalDate,
            departureDate: departure || this.dates.departureDate,
        }
    }

    setArrivalSelectedAlready(isSelected: boolean): void {
        this.arrivalSelectedAlready = isSelected
    }

    setDepartureSelectedAlready(isSelected: boolean): void {
        this.departureSelectedAlready = isSelected
    }
}
